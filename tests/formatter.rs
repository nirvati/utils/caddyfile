// Copyright 2015 Matthew Holt and The Caddy Authors
// Copyright 2023 Aaron Dewes and The Nirvati Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use caddyfile::formatter::format;

use pretty_assertions::assert_eq;

#[test]
fn very_simple() {
    let input = r#"abc   def
	g hi jkl
mn"#;
    let expect = r#"abc def
g hi jkl
mn
"#;
    assert_eq!(format(input), expect);
}

#[test]
fn basic_indentation_line_breaks_and_nesting() {
    let input = r#"  a
	b
	
		c {
			d
	}
	
	e { f
	}
	
	
	
	g {
	h {
	i
	}
	}
	
	j { k {
	l
	}
	}
	
	m {
		n { o
		}
		p { q r
	s }
	}
	
		{
	{ t
			u
	
		v
	
	w
	}
	}"#;
    let expect = r#"a
b

c {
	d
}

e {
	f
}

g {
	h {
		i
	}
}

j {
	k {
		l
	}
}

m {
	n {
		o
	}
	p {
		q r
		s
	}
}

{
	{
		t
		u

		v

		w
	}
}
"#;
    assert_eq!(format(input), expect);
}

#[test]
fn block_spacing() {
    let input = r#"a{
	b
}

c{ d
}"#;
    let expect = r#"a {
	b
}

c {
	d
}
"#;
    assert_eq!(format(input), expect);
}

#[test]
fn advanced_spacing() {
    let input = r#"abc {
		def
	}ghi{
		jkl mno
	pqr}"#;
    let expect = r#"abc {
	def
}

ghi {
	jkl mno
	pqr
}
"#;
    assert_eq!(format(input), expect);
}

#[test]
fn env_var_placeholders() {
    let input = r#"{$A}

b {
{$C}
}

d { {$E}
}

{ {$F}
}"#;
    let expect = r#"{$A}

b {
	{$C}
}

d {
	{$E}
}

{
	{$F}
}
"#;
    assert_eq!(format(input), expect);
}

#[test]
fn env_var_placeholders_with_port() {
    let input = ":{$PORT}";
    let expect = ":{$PORT}\n";
    assert_eq!(format(input), expect);
}

#[test]
fn comments() {
    let input = r#"#a "\n"

#b {
	c
}

d {
e#f
# g
}

h { # i
}"#;
    let expect = r#"#a "\n"

#b {
c
}

d {
	e#f
	# g
}

h {
	# i
}
"#;
    assert_eq!(format(input), expect);
}

#[test]
fn quotes_and_escaping() {
    let input = r##""a \"b\" "#c
	d

e {
"f"
}

g { "h"
}

i {
	"foo
bar"
}

j {
"\"k\" l m"
}"##;
    let expect = r##""a \"b\" "#c
d

e {
	"f"
}

g {
	"h"
}

i {
	"foo
bar"
}

j {
	"\"k\" l m"
}
"##;
    assert_eq!(format(input), expect);
}

#[test]
fn bad_nesting_too_many_open() {
    let input = r#"a
{
	{
}"#;
    let expect = r#"a {
	{
	}
"#;
    assert_eq!(format(input), expect);
}

#[test]
fn bad_nesting_too_many_close() {
    let input = r#"a
{
	{
}}}"#;
    let expect = r#"a {
	{
	}
}
}
"#;
    assert_eq!(format(input), expect);
}

#[test]
fn json() {
    let input = r#"foo
	bar      "{\"key\":34}"
"#;
    let expect = r#"foo
bar "{\"key\":34}"
"#;
    assert_eq!(format(input), expect);
}

#[test]
fn escaping_after_spaces() {
    let input = r#"foo \"literal\""#;
    let expect = r#"foo \"literal\"
"#;
    assert_eq!(format(input), expect);
}

#[test]
fn simple_placeholders_as_standalone_tokens() {
    let input = r#"foo {bar}"#;
    let expect = r#"foo {bar}
"#;
    assert_eq!(format(input), expect);
}

#[test]
fn simple_placeholders_within_tokens() {
    let input = r#"foo{bar} foo{bar}baz"#;
    let expect = r#"foo{bar} foo{bar}baz
"#;
    assert_eq!(format(input), expect);
}

#[test]
fn placeholders_and_malformed_braces() {
    let input = r#"foo{bar} foo{ bar}baz"#;
    let expect = r#"foo{bar} foo {
	bar
}

baz
"#;
    assert_eq!(format(input), expect);
}

#[test]
fn hash_within_string_is_not_a_comment() {
    let input = r#"redir / /some/#/path"#;
    let expect = r#"redir / /some/#/path
"#;
    assert_eq!(format(input), expect);
}

#[test]
fn brace_does_not_fold_into_comment_above() {
    let input = r#"# comment
{
	foo
}"#;
    let expect = r#"# comment
{
	foo
}
"#;
    assert_eq!(format(input), expect);
}

// matthewpi/vscode-caddyfile-support#13
#[test]
fn issue_13() {
    let input = r#"{
email {$ACMEEMAIL}
#debug
}

block {
}
"#;
    let expect = r#"{
	email {$ACMEEMAIL}
	#debug
}

block {
}
"#;
    assert_eq!(format(input), expect);
}

#[test]
fn issue_13_bad_formatting() {
    let input = r#"{
email {$ACMEEMAIL}
#debug
}

block {
}
"#;
    let expect = r#"{
	email {$ACMEEMAIL}
	#debug
}

block {
}
"#;
    assert_eq!(format(input), expect);
}

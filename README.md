# Caddyfile for Rust

This is a library for working with Caddyfiles. It currently only 

Most of this code is a port of the original Go implementation, which is available at github.com/mholt/caddy/tree/master/caddyfile, to equivalent Rust code.

Because of this, every part should have exactly the same behavior as the original, and it should be able to format and tokenize any Caddyfile in the same way Caddy does.

We also try to test all code against the original's test suite to make sure any compatibility issues are caught.
